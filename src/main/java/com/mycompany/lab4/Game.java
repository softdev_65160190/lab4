/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {

    private Table table;
    private Player player1, player2;

    public Game() {
        player1 = new Player('O');
        player2 = new Player('X');
    }

    public void play() {
        newGame();
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputNumber();
            if (table.checkWin()) {
                showTable();
                saveWin();
                showWin();
                if (checkContinue() == 'y') {
                    newGame();
                    continue;
                }
            }
            if (table.checkDraw()) {
                showTable();
                showDraw();
                saveDraw();
                if (checkContinue() == 'y') {
                    newGame();
                    continue;
                }
                break;
            }
            table.switchPlayer();
        }

    }

    private void saveWin() {
        if (table.getCurrentPlayer() == player1.getSymbol()) {
            player1.win();
            player2.lose();
        } else {
            player1.lose();
            player2.win();
        }
    }

    private void saveDraw() {
        player1.draw();
        player2.draw();
    }

    private void showWelcome() {
        System.out.println("Welcome to OX Game;");
    }
    
    public void showWin(){
        System.out.println(table.getCurrentPlayer()+"Win!");
    }
    
    public void showDraw(){
        System.out.println("Draw");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println();
        }

    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void inputNumber() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input Number: ");
        char num = sc.next().charAt(0);
        table.setInput(num);
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer() + " Turn");
    }

    private char checkContinue() {
        Scanner sc = new Scanner(System.in);
        char YesNo = sc.next().charAt(0);
        while (YesNo != 'y' && YesNo != 'n') {
            System.out.println("Enter y or n");
            System.out.println("Do you want to continue?(y or n)");
            YesNo = sc.next().charAt(0);
        }
        return YesNo;
    }
}
