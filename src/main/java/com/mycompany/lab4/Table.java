/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author informatics
 */
public class Table {

    private char[][] table = {{'7', '8', '9'},
    {'4', '5', '6'},
    {'1', '2', '3'}};
    private Player player1;
    private Player player2;
    private Player currentPlayer;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public char[][] getTable() {
        return table;
    }

    public void setInput(char num) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (num == table[i][j]) {
                    table[i][j] = currentPlayer.getSymbol();
                }
            }
        }
    }

    public boolean checkWin() {
        return checkRow()||checkCol()||checkX1()||checkX2();
    }

    public boolean checkDraw() {
        for(int row=0;row<3;row++){
            for(int col=0;col<3;col++){
                if(table[row][col]!='X' && table[row][col]!='O'){
                    return false;
                }
            }
        }
        return true;
    }


    private boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == currentPlayer.getSymbol() && table[i][1] == currentPlayer.getSymbol() && table[i][2] == currentPlayer.getSymbol()) {
                return true;
            }
        }
        return false;
    }

    private boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[0][i] == currentPlayer.getSymbol() && table[1][i] == currentPlayer.getSymbol() && table[2][i] == currentPlayer.getSymbol()) {
                return true;
            }
        }
        return false;
    }


    private boolean checkX1() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }


    private boolean checkX2() {
        for(int i = 0; i < 3; i++){
            if(table[i][table.length - 1 - i] != currentPlayer.getSymbol()){
                return false;
            }
        }
        return true;
    }


    public char getCurrentPlayer() {
        return currentPlayer.getSymbol();
    }

    public void switchPlayer() {
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }
}
